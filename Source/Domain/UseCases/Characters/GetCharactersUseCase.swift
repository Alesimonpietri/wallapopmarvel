//
//  GetCharactersUseCase.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation
import RxSwift

protocol GetCharactersUseCaseProtocol {
  func getCharacters() -> Single<[Character]>
}

class GetCharactersUseCase {
  let charactersRepo: CharactersRepositoryProtocol

  init(charactersRepo: CharactersRepositoryProtocol) {
    self.charactersRepo = charactersRepo
  }
}

extension GetCharactersUseCase: GetCharactersUseCaseProtocol {
  func getCharacters() -> Single<[Character]> {
    charactersRepo.fetchCharacters()
  }
}

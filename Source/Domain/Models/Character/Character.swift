//
//  Character.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation

class Character: NSObject {
  let id: Int
  let name: String
  let des: String
  let imageUrl: String

  init(id: Int, name: String, description: String, imageUrl: String) {
    self.id = id
    self.name = name
    self.des = description
    self.imageUrl = imageUrl
  }
}

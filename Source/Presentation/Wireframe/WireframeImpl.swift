import UIKit

class WireframeImpl: Wireframe {

  // MARK: - Splash

  func splash() -> Screen {
    let vc = SplashViewController<SplashPresenterImpl>.instantiate()
    return Screen(viewController: vc)
  }

  // MARK: - TabBar

  func mainTabBar() -> Screen {
    let imageConfig = UIImage.SymbolConfiguration(scale: .large)

    // MARK: CharactersList
    let charactersListVC = charactersList().get()
    let charactersListIcon = UIImage(systemName: "person.fill", withConfiguration: imageConfig)
    charactersListVC.tabBarItem = UITabBarItem(title: nil, image: charactersListIcon, tag: 0)

    let tabVC = MainTabBarController(vcs: [charactersListVC])
    return Screen(viewController: tabVC)
  }

  // MARK: - Characters

  func charactersList() -> Screen {
    let vc = CharactersListViewController<CharactersListPresenter>.instantiate()
    return Screen(viewController: vc)
  }
}

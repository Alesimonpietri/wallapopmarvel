import Foundation
import RxSwift
import UIKit

protocol BasePresenterProtocol: AnyObject {
  associatedtype View: BaseView
  var view: View? { set get }
  
  var wireframe: Wireframe? { set get }

  func attachView<View>(view: View)

  func viewDidLoad()
  func viewWillAppear()
  func viewDidAppear()
}

extension BasePresenterProtocol {
  func viewDidLoad() {}
  func viewWillAppear() {}
  func viewDidAppear() {}

  func attachView<View>(view: View) {
    self.view = view as? Self.View
  }
}

protocol BaseErrorHandler {
  func handle(error: DomainError, view: BaseView?)
}

extension BaseErrorHandler {
  func handle(error: DomainError, view: BaseView?) {
    switch error {
      case .notConnectedToInternet:
        view?.showNativeAlert(title: "Connection Error", message: nil, completion:  nil)
      case .error(let message):
        view?.showNativeAlert(title: "Error", message: message, completion:  nil)
      default:
        view?.showNativeAlert(title: "unknown", message: nil, completion:  nil)
    }
  }
}

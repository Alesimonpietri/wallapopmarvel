import Foundation
import UIKit

@objc protocol BaseView: AnyObject {
    func showLoading()
    func showSuccess(completion: ((Bool) -> Void)?)
    func hideLoading()

    func showNativeAlert(title: String, message: String?, completion: ((UIAlertAction) -> Void)?)
}

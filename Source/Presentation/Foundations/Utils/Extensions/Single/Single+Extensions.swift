import RxSwift
import Moya

enum NetworkError: Int {
  case notFound = 404
  case forbidden = 403
  case serverError = 500
}

extension PrimitiveSequence where Trait == SingleTrait {

  func loading(view: BaseView?) -> Single<Element> {
    view?.showLoading()
    return self.do(onSuccess: { _ in view?.hideLoading()},
                   onError: { _ in view?.hideLoading()})
  }

  func handleError(_ handler: BaseErrorHandler, view: BaseView?) -> Single<Element> {
    return self.do(onError: { error in
      switch error {
        case let moyaError as MoyaError:
          self.handleMoyaError(moyaError, completion: { domainError in
            handler.handle(error: domainError, view: view)
          })
        default:
          handler.handle(error: .someError, view: view)
      }
    })
  }

  private func handleMoyaError(_ moyaError: MoyaError, completion: (DomainError) -> Void) {
    guard case .statusCode(let response) = moyaError else {
      print("No hay conexion")
      completion(.notConnectedToInternet)
      return
    }

    guard let error = NetworkError(rawValue: response.statusCode) else {
      completion(.someError)
      return
    }

    switch error {
      case .notFound:
        completion(.error(message: "Not Found"))
      case .forbidden:
        completion(.error(message: "Forbidden"))
      case .serverError:
        completion(.error(message: "Server Error"))
    }
  }
}

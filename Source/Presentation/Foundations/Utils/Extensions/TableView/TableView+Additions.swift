import UIKit

extension UITableView {
    func registerCellNib(cell type: UITableViewCell.Type, bundle: Bundle? = nil) {
        let className = String(describing: type)
        register(UINib(nibName: className, bundle: bundle), forCellReuseIdentifier: className)
    }

    func registerCell(cell type: UITableViewCell.Type, bundle: Bundle? = nil) {
        let className = String(describing: type)
        register(type, forCellReuseIdentifier: className)
    }
    
    func dequeueReusableBy<T>(cell type: T.Type, indexPath: IndexPath) -> T {
        let identifier = String(describing: type)
        return dequeueReusableCell(withIdentifier: identifier, for: indexPath) as! T
    }
}

extension UITableView {

    func selectAll(animated: Bool = false) {
        let totalSections = self.numberOfSections
        for section in 0 ..< totalSections {
            let totalRows = self.numberOfRows(inSection: section)
            for row in 0 ..< totalRows {
                let indexPath = IndexPath(row: row, section: section)
                // call the delegate's willSelect, select the row, then call didSelect
                self.delegate?.tableView?(self, willSelectRowAt: indexPath)
                self.selectRow(at: indexPath, animated: animated, scrollPosition: .none)
                self.delegate?.tableView?(self, didSelectRowAt: indexPath)
            }
        }
    }

    func deSelectAll(animated: Bool = false) {
        let totalSections = self.numberOfSections
        for section in 0 ..< totalSections {
            let totalRows = self.numberOfRows(inSection: section)
            for row in 0 ..< totalRows {
                let indexPath = IndexPath(row: row, section: section)
                // call the delegate's willSelect, select the row, then call didSelect
                self.delegate?.tableView?(self, willSelectRowAt: indexPath)
                self.selectRow(at: indexPath, animated: animated, scrollPosition: .none)
                self.delegate?.tableView?(self, didDeselectRowAt: indexPath)
            }
        }
    }

}

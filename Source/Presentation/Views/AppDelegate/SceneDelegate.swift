//
//  SceneDelegate.swift
//  KioskoTibaan
//
//  Created by  on 21/8/20  - W34.
//

import UIKit

class SceneDelegate: UIResponder, UIWindowSceneDelegate {

    var window: UIWindow?

    let wireframe: Wireframe = {
        return DependecyInjection.shared.instantiate(type: Wireframe.self)
    }()

    func scene(_ scene: UIScene, willConnectTo session: UISceneSession, options connectionOptions: UIScene.ConnectionOptions) {
        guard let windowScene = (scene as? UIWindowScene) else { return }
        let newWindow = UIWindow(windowScene: windowScene)
        newWindow.rootViewController = wireframe.splash().get()
        window = newWindow
        window?.overrideUserInterfaceStyle = .light
        newWindow.makeKeyAndVisible()
    }
}


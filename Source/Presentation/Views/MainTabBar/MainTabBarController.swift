//
//  MainTabBarController.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import UIKit

class MainTabBarController: UITabBarController {
  init(vcs: [UIViewController]) {
    super.init(nibName: nil, bundle: nil)
    self.viewControllers = vcs
    setup()
  }

  required init?(coder aDecoder: NSCoder) {
    super.init(coder: aDecoder)
    setup()
  }

  override func viewDidLoad() {
    super.viewDidLoad()
  }

  private func setup() {
    tabBar.barTintColor = UIColor.TabBar.background
    tabBar.tintColor = UIColor.TabBar.selectedItem
    tabBar.isTranslucent = false
  }
}

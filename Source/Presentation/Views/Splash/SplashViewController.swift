import UIKit

class SplashViewController<P: SplashPresenter>: BaseViewController<P>, SplashView {
  override func viewDidAppear(_ animated: Bool) {
    super.viewDidAppear(animated)
    view.backgroundColor = .white
  }
}

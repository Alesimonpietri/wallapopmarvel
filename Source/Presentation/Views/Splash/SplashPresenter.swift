import Foundation

@objc protocol SplashView: BaseView {}

protocol SplashPresenter: BasePresenterProtocol {}

class SplashPresenterImpl: SplashPresenter {
  var view: SplashView?
  var wireframe: Wireframe?

  func viewDidAppear() {
    firstNavigation()
  }
}

private extension SplashPresenterImpl {
  func firstNavigation() {
    wireframe?
      .mainTabBar()
      .replaceAppRootViewController()
  }
}

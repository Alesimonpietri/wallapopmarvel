//
//  CharactersListViewController.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import UIKit

class CharactersListViewController<P: CharactersListPresenterProtocol>: BaseViewController<P>,
                                                                        CharactersListView,
                                                                        UICollectionViewDataSource,
                                                                        UICollectionViewDelegateFlowLayout {
  // MARK: - Views

  lazy var collectionView: UICollectionView = {
    let layout = UICollectionViewFlowLayout()
    layout.scrollDirection = .vertical
    layout.minimumInteritemSpacing = 0
    let collectionView = UICollectionView(frame: .zero, collectionViewLayout: layout)
    collectionView.dataSource = self
    collectionView.delegate = self
    collectionView.registerCell(cell: CharacterViewCell.self)
    return collectionView
  }()

  // MARK: - Vars

  let edge: UIEdgeInsets = UIEdgeInsets(top: 8, left: 8, bottom: 8, right: 8)
  var characters: [Character] = []

  // MARK: - Lifecycle

  override func loadView() {
    view = collectionView
  }

  override func viewDidLoad() {
    super.viewDidLoad()
    setup()
  }

  // MARK: - CharactersListView
  func update(characters: [Character]) {
    self.characters = characters
    collectionView.reloadData()
  }

  // MARK: - UICollectionViewDataSource

  func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
    return characters.count
  }

  func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
    let cell = collectionView.dequeueReusableBy(cell: CharacterViewCell.self, indexPath: indexPath)
    let character = characters[indexPath.item]
    cell.configView(character: character)
    return cell
  }

  // MARK: - UICollectionViewDelegateFlowLayout

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, insetForSectionAt section: Int) -> UIEdgeInsets {
    return edge
  }

  func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize {
    let colums: CGFloat = 2
    let width = (collectionView.frame.width - edge.right)/colums - (edge.right)
    let height = (collectionView.frame.height * 0.35) - edge.top - edge.bottom
    return CGSize(width: width, height: height)
  }
}

private extension CharactersListViewController {
  func setup() {
    view.backgroundColor = UIColor.Base.background
  }

  func applyConstraints() {
    // not implemented yet
  }
}

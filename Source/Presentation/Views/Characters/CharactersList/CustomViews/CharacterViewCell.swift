//
//  CharacterViewCell.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import UIKit

class CharacterViewCell: UICollectionViewCell {

  lazy var stackView: UIStackView = {
    let stack = UIStackView(arrangedSubviews: [imageView,
                                               titleLabel])
    stack.axis = .vertical
    return stack
  }()

  let imageView: UIImageView = {
    let imageView = UIImageView()
    imageView.image = UIImage(systemName: "person")
    imageView.tintColor = .white
    imageView.contentMode = .scaleAspectFill
    imageView.clipsToBounds = true
    return imageView
  }()

  let titleLabel: UILabel = {
    let label = UILabel()
    label.bold()
    label.numberOfLines = 2
    label.textAlignment = .center
    label.setContentCompressionResistancePriority(.required, for: .vertical)
    return label
  }()

  override init(frame: CGRect) {
    super.init(frame: frame)
    setup()
    applyConstraints()
  }

  @available(*, unavailable)
  required public init?(coder: NSCoder) {
    fatalError()
  }

  func configView(character: Character) {
    titleLabel.text = character.name
    imageView.setImage(stringURL: character.imageUrl)
  }
}

private extension CharacterViewCell {
  func setup() {
    addAutoLayout(subview: stackView)
    rounded(radius: 16)
    backgroundColor = .red
  }

  func applyConstraints() {
    addAllEdgesConstraint(to: stackView)
  }
}

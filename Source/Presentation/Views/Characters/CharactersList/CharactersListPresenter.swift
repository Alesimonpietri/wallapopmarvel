//
//  CharactersListPresenter.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation
import RxSwift

@objc protocol CharactersListView: BaseView {
  func update(characters: [Character])
}

protocol CharactersListPresenterProtocol: BasePresenterProtocol {}

class CharactersListPresenter: CharactersListPresenterProtocol, BaseErrorHandler {

  private let getCharactersUseCase: GetCharactersUseCaseProtocol
  private let schedulers: Schedulers
  private let disposeBag: DisposeBag

  var view: CharactersListView?
  var wireframe: Wireframe?

  init(schedulers: Schedulers, getCharactersUseCase: GetCharactersUseCaseProtocol) {
    self.schedulers = schedulers
    self.getCharactersUseCase = getCharactersUseCase
    self.disposeBag = DisposeBag()
  }

  func viewWillAppear() {
    getCharacters()
  }
}

private extension CharactersListPresenter {
  func getCharacters() {
    getCharactersUseCase.getCharacters()
      .applySchedulers(schedulers: schedulers)
      .loading(view: view)
      .handleError(self, view: view)
      .subscribe(onSuccess: { [weak self] characters in
        self?.view?.update(characters: characters)
      }).disposed(by: disposeBag)
  }
}

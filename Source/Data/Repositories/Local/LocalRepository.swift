import Foundation

struct LocalKeys {
    static let token = "access_token"
}

class LocalRepository {

    let userDefaults = UserDefaults.standard
    
    // MARK: - Token
    
    func saveToken(token: String) {
        userDefaults.set(token, forKey: LocalKeys.token)
    }
    
    static func getToken() -> String? {
        guard let token = UserDefaults.standard.object(forKey: LocalKeys.token) as? String, !token.isEmpty else {
            return nil
        }
        
        return token
    }
    
    func deleteToken() {
        userDefaults.removeObject(forKey: LocalKeys.token)
    }
}

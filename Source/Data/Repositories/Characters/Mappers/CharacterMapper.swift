//
//  CharacterMapper.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation

class CharacterMapper {
  static func map(response: CharacterResponse?) -> Character? {
    guard let id = response?.id,
          let name = response?.name,
          let des = response?.resultDescription,
          let imageUrl = getImageUrl(from: response?.thumbnail) else {
      return nil
    }
    return Character(id: id,
                     name: name,
                     description: des,
                     imageUrl: imageUrl)
  }

  private static func getImageUrl(from thumbnail: ThumbnailResponse?) -> String? {
    guard let path = thumbnail?.path, let ext = thumbnail?.thumbnailExtension?.rawValue else {
      return nil
    }
    return "\(path).\(ext)"
  }
}

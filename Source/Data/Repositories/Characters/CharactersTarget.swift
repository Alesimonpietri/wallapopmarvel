//
//  CharactersTarget.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation
import Moya

enum CharactersTarget {
  case characters
}

extension CharactersTarget: TargetType {
  var baseURL: URL {
    return URL(string: EnviromentConfig.shared.baseUrl)!
  }

  var path: String {
    switch self {
      case .characters:
        return "/characters"
    }
  }

  var method: Moya.Method {
    switch self {
      case .characters:
        return .get
    }
  }

  var task: Task {
    switch self {
      case .characters:
        return .requestPlain
    }
  }

  var headers: [String : String]? {
    return nil
  }

  var sampleData: Data {
    return Data()
  }
}

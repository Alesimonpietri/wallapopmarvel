//
//  MarvelResponse.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation

// MARK: - MarvelResponse
struct MarvelResponse<T: Codable>: Codable {
  let code: Int?
  let status: String?
  let etag: String?
  let data: MarvelData<T>?

  enum CodingKeys: String, CodingKey {
    case code = "code"
    case status = "status"
    case etag = "etag"
    case data = "data"
  }
}

// MARK: - DataClass
struct MarvelData<T: Codable>: Codable {
  let offset: Int?
  let limit: Int?
  let total: Int?
  let count: Int?
  let results: [T]?

  enum CodingKeys: String, CodingKey {
    case offset = "offset"
    case limit = "limit"
    case total = "total"
    case count = "count"
    case results = "results"
  }
}

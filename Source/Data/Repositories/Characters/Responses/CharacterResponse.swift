//
//  CharacterResponse.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation

// MARK: - Result
struct CharacterResponse: Codable {
  let id: Int?
  let name: String?
  let resultDescription: String?
  let thumbnail: ThumbnailResponse?

  enum CodingKeys: String, CodingKey {
    case id = "id"
    case name = "name"
    case resultDescription = "description"
    case thumbnail = "thumbnail"
  }
}

// MARK: - Thumbnail
struct ThumbnailResponse: Codable {
  let path: String?
  let thumbnailExtension: ExtensionResponse?

  enum CodingKeys: String, CodingKey {
    case path = "path"
    case thumbnailExtension = "extension"
  }
}

enum ExtensionResponse: String, Codable {
  case gif = "gif"
  case jpg = "jpg"
}

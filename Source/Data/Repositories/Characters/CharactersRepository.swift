//
//  CharactersRepository.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation
import RxSwift

protocol CharactersRepositoryProtocol {
  func fetchCharacters() -> Single<[Character]>
}

class CharactersRepository: BaseRepository<CharactersTarget>, CharactersRepositoryProtocol {
  func fetchCharacters() -> Single<[Character]> {
    request(.characters)
      .filterSuccessfulStatusCodes()
      .map(MarvelResponse<CharacterResponse>.self)
      .map { response in
        return response.data?.results?.compactMap { CharacterMapper.map(response: $0) } ?? []
      }
  }
}

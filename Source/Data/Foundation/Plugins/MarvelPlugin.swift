//
//  MarvelPlugin.swift
//  WallapopMarvel
//
//  Created by Alejandro Simonpietri on 28/11/21.
//

import Foundation
import Moya
import CryptoKit

class MarvelPlugin: PluginType {

  private enum Keys {
    static let publicKey = "99a827933f068de962e203485b61fee0"
    static let privateKey = "7ddcd5eb91f26310d91b9a55f6b5c7d3f5464c7a"
  }

  func prepare(_ request: URLRequest, target: TargetType) -> URLRequest {
    var marvelRequest = request
    guard let url = marvelRequest.url,
          var urlComponents = URLComponents(url: url, resolvingAgainstBaseURL: true) else {
      return request
    }

    prepareMarvel(urlComponents: &urlComponents)
    marvelRequest.url = urlComponents.url?.absoluteURL ?? url
    return marvelRequest
  }
}

private extension MarvelPlugin {
  func getHash(timestamp: String) -> String {
    let message = "\(timestamp)\(Keys.privateKey)\(Keys.publicKey)"
    guard let data = message.data(using: .utf8) else { return "" }
    return "\(data.md5)"
  }

  func prepareMarvel(urlComponents: inout URLComponents) {
    let timestamp = Date().timeIntervalSince1970.description
    let queryItems: [URLQueryItem] = [URLQueryItem(name: "apikey", value: Keys.publicKey),
                                      URLQueryItem(name: "ts", value: timestamp),
                                      URLQueryItem(name: "hash", value: getHash(timestamp: timestamp))]
    if urlComponents.queryItems != nil {
      urlComponents.queryItems?.append(contentsOf: queryItems)
    } else {
      urlComponents.queryItems = queryItems
    }
  }
}

private extension Data {
  var md5: String {
    Insecure.MD5
      .hash(data: self)
      .map {String(format: "%02x", $0)}
      .joined()
  }
}

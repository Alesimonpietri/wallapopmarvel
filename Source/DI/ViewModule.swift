import Foundation
import Swinject

class ViewModule {
  static func setup(_ defaultContainer: Container) {

    defaultContainer.register(Wireframe.self) { _ in
      WireframeImpl()
    }.inObjectScope(.container)

    resolvePresenters(defaultContainer)
    resolveViewControllers(defaultContainer)
  }

  // MARK: - Presenters

  static func resolvePresenters(_ defaultContainer: Container) {

    //MARK:  Splash
    defaultContainer.register(SplashPresenterImpl.self) { r in
      SplashPresenterImpl()
    }

    //MARK:  Characters
    defaultContainer.register(CharactersListPresenter.self) { r in
      CharactersListPresenter(schedulers: r.resolve(Schedulers.self)!,
                              getCharactersUseCase: r.resolve(GetCharactersUseCaseProtocol.self)!)
    }
  }

  // MARK: - ViewControllers

  static func resolveViewControllers(_ defaultContainer: Container) {
    // To make your life easier
    func register<P, V: BaseViewController<P>>(vc: V.Type) {
      defaultContainer.register(vc) { r in
        let instantiateVC = vc.init()
        instantiateVC.presenter = r.resolve(V.Presenter.self)!
        instantiateVC.presenter.wireframe = r.resolve(Wireframe.self)!
        return instantiateVC
      }
    }

    // MARK: Splash
    register(vc: SplashViewController<SplashPresenterImpl>.self)

    //MARK:  Characters
    register(vc: CharactersListViewController<CharactersListPresenter>.self)
  }
}

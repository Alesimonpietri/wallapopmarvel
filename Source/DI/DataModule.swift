import Foundation
import Swinject

class DataModule {
  
  static func setup(_ defaultContainer: Container) {
    defaultContainer.register(Schedulers.self) { _ in
      Schedulers()
    }.inObjectScope(.container)
    
    resolveRepositories(defaultContainer)
    resolveUseCase(defaultContainer)
  }
  
  static func resolveRepositories(_ defaultContainer: Container) {
    func getAPIProvider<T>() -> APIProvider<T> {
      return APIProvider(schedulers: defaultContainer.resolve(Schedulers.self)!)
    }
    
    defaultContainer.register(LocalRepository.self) { _ in
      LocalRepository()
    }.inObjectScope(.container)
    
    
    defaultContainer.register(CharactersRepositoryProtocol.self) { r in
      CharactersRepository(provider: getAPIProvider())
    }
  }
  
  static func resolveUseCase(_ defaultContainer: Container) {
    //MARK:  Characters
    defaultContainer.register(GetCharactersUseCaseProtocol.self) { r in
      GetCharactersUseCase(charactersRepo: r.resolve(CharactersRepositoryProtocol.self)!)
    }
  }
}
